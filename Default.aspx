﻿<%@ Page Title="" Language="C#" MasterPageFile="~/w_NAS.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AppNAS.GestorDeRestaurantes.Default" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- About Section -->
    <section id="about" class="about content-section alt-bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="padding-top: 50px" itemprop="description">
                    <h2>Atendimento &amp; Cardápio Digital</h2>
                    <h3 class="glyphicon-book gray">Utilize o Gestor de Restaurantes para Modernizar, Agilizar e Lucrar +</h3>
                    <p>Modernizar o seu negócio sem precisar investir em equipamentos como: tablets e smartphones. O Gestor de Restaurantes se adapta aos celulares que você, seus funcionários e clientes já possuem com acesso à internet ou Wi-Fi. </p>
                    <p>Os clientes podem usar aparelhos móveis ou qualquer dispositivo com acesso à internet para realizar pedidos diretamente à sua cozinha através de seu site.</p>
                </div>
                <!-- /.col-md-6 -->

                <blockquote>
                    <p style="text-align: right;">Agilidade e confiança, tenha todo o histórico de atendimento e pedidos, controle tudo de forma simples e segura. Diminua falhas operacionais, centralize e garanta o controle de pedidos e entregas. Agilize o atendimento e aumente a confiança de seu cliente.</p>
                </blockquote>
                <div class="col-md-6">
                    <!-- Display image -->
                    <!-- <img src="http://gestorderestaurantes.com.br/c_estilo/imagens/hallooou_template-iphone_sm.png" class="img-responsive"> -->
                    <!-- Optional video embed code -->
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe src="https://player.vimeo.com/video/152553862?title=0&amp;byline=0&amp;portrait=1" class="embed-responsive-item" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /.section -->

    <!-- Call to action - two section -->
    <section class="cta-two-section cta-two-section2">
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <h3>Curta e Compartilhe?</h3>
                    <p>Acesse nosso face siga nossa fanpage e fique sempre atualizado!</p>
                </div>
                <div class="col-sm-3">
                    <a href="http://facebook.com/gestorderestaurantes" target="_blank" class="btn btn-overcolor">facebook</a>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- Services Section -->
    <section id="services" class="services content-section">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <h2>Completo e Inovador</h2>
                    <h3 class="caption gray">Emissão Fiscal, Pedido Online, Agilidade para os Garçons, Segurança para o Gestor, Aumento dos Lucros e Cliente Satisfeito.</h3>
                </div>
                <!-- /.col-md-12 -->

                <div class="container">
                    <div class="row text-center">
                        <div class="col-md-4">
                            <div class="row services-item sans-shadow text-center">
                                <i class="fa fa-laptop fa-3x"></i>
                                <h4>Seu Site</h4>
                                <p>O Gestor de Restaurantes cria um site único para sua empresa. Domínio e e-mail próprio.</p>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.col-md-4 -->

                        <div class="col-md-4">
                            <div class="row services-item sans-shadow text-center">
                                <i class="fa fa-mobile fa-3x"></i>
                                <h4>Atendimento Eletrônico</h4>
                                <p>Seu funcionário atende através de qualquer aparelho (tablet, smartphone,etc) ligado à internet. Sem erros, mais agilidade e melhor controle de estoque.</p>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.col-md-4 -->

                        <div class="col-md-4">
                            <div class="row services-item sans-shadow text-center">
                                <i class="fa fa-shopping-basket fa-3x"></i>
                                <h4>Cardápio Eletrônico</h4>
                                <p>Os clientes podem efetuar pedidos através de seu site exclusivo. Redução de pessoal e melhoria no serviço.</p>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.col-md-4 -->

                        <div class="col-md-4">
                            <div class="row services-item sans-shadow text-center">
                                <i class="fa fa-print fa-3x"></i>
                                <h4>Impressão Sem Fio</h4>
                                <p>Os pedidos são enviados para uma impressora conforme configuração. Exemplo: pedido de porção já sai na cozinha, pedido de bebidas sai no balcão, agilizando o processo de atendimento.</p>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.col-md-4 -->

                        <div class="col-md-4">
                            <div class="row services-item sans-shadow text-center">
                                <i class="fa fa-globe fa-3x"></i>
                                <h4>Acesse de Qualquer Lugar</h4>
                                <p>Você pode gerenciar tudo de qualquer lugar com acesso à internet. Quem está vendendo, quem está comprando, acompanhamento de fluxo financeiro diário.</p>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.col-md-4 -->

                        <div class="col-md-4">
                            <div class="row services-item sans-shadow text-center">
                                <i class="fa fa-group fa-3x"></i>
                                <h4>Multi-Lojas</h4>
                                <p>O Gestor de Restaurantes está pronto para multi-lojas, você controla todos os estabelecimentos com apenas um acesso, sem precisar estar em todos os locais.</p>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.col-md-4 -->

                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container -->

            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /.section -->



    <!-- Products Section -->
    <section id="products" class="products content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <video muted autoplay loop width="400" height="500" class="fill">
                        <source src="c_estilo/videos/mobile.mp4" type="video/mp4">
                        <source src="c_estilo/videos/mobile.mp4" type="video/mp4">
                    </video>
                </div>
                <!-- /.col-md-4 -->

                <div class="col-md-8">
                    <div class="products-container">

                        <div class="col-md-12">
                            <h2>Como Funciona?</h2>
                            <h3 class="caption white">Muito além de controle de estoque, gestão de caixa e conta de cliente...</h3>
                        </div>
                        <!-- /.col-md-12 -->

                        <div class="col-md-6 product-item">
                            <div class="media-left">
                                <span class="icon"><i class="fa fa-1 fa-3x"></i></span>
                            </div>
                            <!-- /.media-left -->
                            <div class="media-body">
                                <h3 class="media-heading">Garçom ou Cliente</h3>
                                <p>O garçom ou cliente faz o pedido por smartphone, tablet ou qualquer aparelho com acesso à internet. (Cliente via site)</p>
                            </div>
                            <!-- /.media-body -->
                        </div>
                        <!-- /.col-md-6 -->

                        <div class="col-md-6 product-item">
                            <div class="media-left">
                                <span class="icon"><i class="fa fa-2 fa-3x"></i></span>
                            </div>
                            <!-- /.media-left -->
                            <div class="media-body">
                                <h3 class="media-heading">O Pedido é Enviado</h3>
                                <p>Para a cozinha, balcão ou local configurado pelo gestor por tipo de produto ou categoria solicitada. Impresso e exibição em monitores.</p>
                            </div>
                            <!-- /.media-body -->
                        </div>
                        <!-- /.col-md-6 -->

                        <div class="col-md-6 product-item">
                            <div class="media-left">
                                <span class="icon"><i class="fa fa-3 fa-3x"></i></span>
                            </div>
                            <!-- /.media-left -->
                            <div class="media-body">
                                <h3 class="media-heading">O Pedido é Preparado</h3>
                                <p>Conforme identificação do tipo (delivery, vem buscar ou mesa). Após o preparo o funcionário responsável pela entrega leva ao cliente.</p>
                            </div>
                            <!-- /.media-body -->
                        </div>
                        <!-- /.col-md-6 -->

                        <div class="col-md-6 product-item">
                            <div class="media-left">
                                <span class="icon"><i class="fa fa-4 fa-3x"></i></span>
                            </div>
                            <!-- /.media-left -->
                            <div class="media-body">
                                <h3 class="media-heading">Finalização da Venda</h3>
                                <p>O Cliente faz o pagamento. Todo o processo é registrado, clientes, atendentes e tempos. Podendo ser emitido Nota do Consumidor.</p>
                            </div>
                            <!-- /.media-body -->
                        </div>
                        <!-- /.col-md-6 -->

                        <div class="col-md-6 product-item">
                            <div class="media-left">
                                <span class="icon"><i class="fa fa-5 fa-3x"></i></span>
                            </div>
                            <!-- /.media-left -->
                            <div class="media-body">
                                <h3 class="media-heading">Gestão</h3>
                                <p>Sem erros de controle de estoque ou de escrita. Controle de todos os setores de qualquer lugar, caso tenha mais de um estabelecimento.</p>
                            </div>
                            <!-- /.media-body -->
                        </div>
                        <!-- /.col-md-6 -->

                        <div class="col-md-6 product-item">
                            <div class="media-left">
                                <span class="icon"><i class="fa fa-6 fa-3x"></i></span>
                            </div>
                            <!-- /.media-left -->
                            <div class="media-body">
                                <h3 class="media-heading">Personalização e Relatórios</h3>
                                <p>O Sistema padrão se adequa a qualquer estabelecimento. Porém permitimos pernosalizações específicas, basta informar com antecedência.</p>
                            </div>
                            <!-- /.media-body -->
                        </div>
                        <!-- /.col-md-6 -->

                    </div>
                    <!-- /.products-container -->
                </div>
                <!-- /.col-md-8 -->

            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /.products -->


    <!-- Call to action - two section -->
    <section class="cta-two-section cta-two-section2">
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <h3>Visite nosso BLOG</h3>
                    <p>Conheça nosso blog, e fique por dentro das novidades da gestão de restaurantes e bares!</p>
                </div>
                <div class="col-sm-3">
                    <a href="http://blog.gestorderestaurantes.com.br/" target="_blank" class="btn btn-overcolor">acessar blog</a>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <!-- Our team Section -->
    <section id="team" class="team content-section">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <h2>Tem <b>+</b></h2>
                    <h3 class="caption gray">Saiba quanto você ganha, gasta e lucra</h3>
                </div>
                <!-- /.col-md-12 -->

                <div class="container">
                    <div class="row">

                        <div class="col-md-4">
                            <div class="team-member">
                                <figure>
                                    <img src="http://gestorderestaurantes.com.br/c_estilo/imagens/estoque.png" alt="" class="img-responsive" width="300" />
                                    <figcaption>
                                        <p>O controle de estoque do Gestor de Restaurante, permite o controle por unidade ou matéria prima. Integrado com as entradas de notas fiscais.</p>
                                        <%--                                        <ul>
                                            <li><a href="http://gestorderestaurantes.com.br/"><i class="fa fa-facebook fa-2x"></i></a></li>
                                            <li><a href="http://gestorderestaurantes.com.br/"><i class="fa fa-twitter fa-2x"></i></a></li>
                                            <li><a href="http://gestorderestaurantes.com.br/"><i class="fa fa-linkedin fa-2x"></i></a></li>
                                        </ul>--%>
                                    </figcaption>
                                </figure>
                                <h4>Controle de Estoque.</h4>
                                <p>Em tempo real.</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                        <!-- /.col-md-4 -->

                        <div class="col-md-4">
                            <div class="team-member">
                                <figure>
                                    <img src="http://gestorderestaurantes.com.br/c_estilo/imagens/acesso.png" alt="" class="img-responsive" width="300" />
                                    <figcaption>
                                        <p>O controle de acesso e auditoria é rígido. Você pode definir os acessos por grupos de funcionários e também listar com facilidade todas as atividades relacionadas a um funcionário ou cliente.</p>
                                    </figcaption>
                                </figure>
                                <h4>Controle de Acesso</h4>
                                <p>Sistema totalmente auditado.</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                        <!-- /.col-md-4 -->

                        <div class="col-md-4">
                            <div class="team-member">
                                <figure>
                                    <img src="http://gestorderestaurantes.com.br/c_estilo/imagens/agenda.png" alt="" class="img-responsive" width="300" />
                                    <figcaption>
                                        <p>O Gestor disponibiliza uma agenda fácil e ágil para manter contato de Fornecedores e Clientes. Dispense papéis, você entra no seu sistema e anota/consulta contatos importantes.</p>
                                    </figcaption>
                                </figure>
                                <h4>Agenda</h4>
                                <p>Contato de Fornecedores e Clientes.</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                        <!-- /.col-md-4 -->


                        <div class="col-md-4">
                            <div class="team-member">
                                <figure>
                                    <img src="http://gestorderestaurantes.com.br/c_estilo/imagens/consumo.png" alt="" class="img-responsive">
                                    <figcaption>
                                        <p>É praticamente impossível os colaboradores não consumirem durante o trabalho. O Gestor de Restaurantes controla os itens consumidos internamente, você saberá o quanto de consumo interno tem por período.</p>
                                    </figcaption>
                                </figure>
                                <h4>Consumo Interno</h4>
                                <p>Controlar itens consumidos internamente.</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                        <!-- /.col-md-4 -->


                        <div class="col-md-4">
                            <div class="team-member">
                                <figure>
                                    <img src="http://gestorderestaurantes.com.br/c_estilo/imagens/fiscal.png" alt="" class="img-responsive" width="300" />
                                    <figcaption>
                                        <p>O Gestor lhe dá as opções de emissões fiscais, podendo integrar NF-e, NFC-e e/ou NFS-e. Seu contador acessa e configura os tributos dos produtos, pronto para emissão. Cada nota emitida é enviada automaticamente para a contabilidade.</p>
                                    </figcaption>
                                </figure>
                                <h4>Emissão Fiscal</h4>
                                <p>Opções de emissão: NF-e, NFC-e e NFS-e.</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                        <!-- /.col-md-4 -->

                        <div class="col-md-4">
                            <div class="team-member">
                                <figure>
                                    <img src="http://gestorderestaurantes.com.br/c_estilo/imagens/caixa.png" alt="" class="img-responsive">
                                    <figcaption>
                                        <p>Simples e Completo você controla todo o fluxo de caixa (entradas e saídas) por caixa, por período. Você terá à sua disposição relatórios padrões ou personalizados para a gestão de fluxos.</p>
                                    </figcaption>
                                </figure>
                                <h4>Fluxo de Caixa</h4>
                                <p>Controle suas entradas de saídas.</p>
                            </div>
                            <!-- /.team-member -->
                        </div>
                        <!-- /.col-md-4 -->

                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container -->

            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /.our-team -->



    <%--    <!-- Portfolio/Gallery Section -->
    <section id="portfolio" class="portfolio content-section parallax">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <h2>Vídeos em destaque</h2>
                    <h3 class="caption white">Assista uma seleção de vídeos úteis para seu negócio. </h3>
                </div>
                <!-- /.col-md-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->

        <div class="container project-container text-center">

            <div class="recent-project-carousel owl-carousel owl-theme popup-gallery">

                <!-- /.item -->
                <div class="item" style="padding: 5px">
                    <video controls width="100%" height="350px" style="background-color: black">
                        <source src="c_estilo/videos/sustentabilidade.mp4" type="video/mp4">
                        <source src="c_estilo/videos/sustentabilidade.mp4" type="video/mp4">
                    </video>
                    <h4 style="background-color: white; padding-top: 15px">Desperdício de Alimentos</h4>
                </div>

                <div class="item" style="padding: 5px">
                    <iframe src="http://tv.sebrae.com.br/api/iframe/?media_id=1995" width="100%" height="350" allowfullscreen frameborder="0"></iframe>
                    <h4 style="background-color: white; padding-top: 15px">Conecte seu negócio e lucre +</h4>
                </div>
                <!-- /.item -->

                <div class="item" style="padding: 5px">
                    <video controls width="100%" height="350px" style="background-color: black">
                        <source src="c_estilo/videos/abrir.mp4" type="video/mp4">
                        <source src="c_estilo/videos/abrir.mp4" type="video/mp4">
                    </video>
                    <h4 style="background-color: white; padding-top: 15px; margin-top: 4px">Deseja abrir um bar ou restaurante?</h4>
                </div>
                <!-- /.item -->

                <div class="item ">
                </div>
                <!-- /.item -->

            </div>
            <!-- /.recent-project-carousel -->

            <div class="customNavigation project-navigation text-center">
                <a class="btn-prev"><i class="fa fa-angle-left fa-2x"></i></a>
                <a class="btn-next"><i class="fa fa-angle-right fa-2x"></i></a>
            </div>
            <!-- /.project-navigation -->

        </div>
        <!-- /.container -->
    </section>
    <!-- /.portfolio -->--%>


    <%--    <section id="clients" class="our-clients content-section text-center">
        <div class="container">
            <div class="row">

                </div>
            </div>
        </section>--%>


    <!-- Call to action - two section -->
    <section class="cta-two-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <h3>Sua opnião é muito importante!</h3>
                    <p>Nos ajude a melhorar o site e nossa comunicação. Participe de nosso questionário de satisfação.</p>
                </div>
                <div class="col-sm-3">
                    <a href="http://gestorderestaurantes.com.br/p_questionario" target="_blank" class="btn btn-overcolor">Participar</a>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /.cta-two-section -->
    <!-- Call to action - one section -->
    <section class="cta-one-section content-section alt-bg-light">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-6">
                    <h2>Nosso atendimento é pessoal e contínuo</h2>
                    <h3 class="caption gray">Caso tenha alguma dúvida entre em contato</h3>
                    <p>Nossa meta é aumentar a lucratividade de nossos clientes, através do aumento de clientes via site ou rede social. Agilizar o atendimento e diminuir o desperdício e perdas. Além de aumentar o controle e gestão. Obrigado pela atenção e estamos anciosos para iniciar uma parceria!</p>

                    <a href="http://gestorderestaurantes.com.br/#contact" onclick='descricaoContato("Dúvidas?", 0);' class="btn btn-default btn-lg">Dúvidas?</a>
                    <a href="http://gestorderestaurantes.com.br/#contact" onclick='descricaoContato("Sugestões!", 1);' class="btn btn-outlined btn-lg">Sugestões?</a>
                </div>
                <!-- /.col-md-6 -->

                <div class="col-md-6">
                    <img src="http://gestorderestaurantes.com.br/c_estilo/imagens/hallooou-responsive.png" class="img-responsive" />
                </div>
                <!-- /.col-md-6 -->

            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /.cta-one-section -->




    <!-- /.cta-two-section -->
    <!-- Counter Section -->
    <section id="counter" class="counter-section content-section">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <h2 class="white">Seja nosso Parceiro</h2>
                    <h3 class="caption">Se você trabalha com vendas, venha ser nosso parceiro. Disponibilizamos: material e treinamento. Seja um vendedor credenciado e tenha exclusividade de venda em sua região!</h3>
                </div>
                <div class="col-sm-12">
                    <a href="http://gestorderestaurantes.com.br/#contact" onclick='descricaoContato("Quer ser uma revenda / parceiro autorizado?", 2);' class="btn btn-overcolor">Quero ser parceiro</a>
                </div>
                <!-- /.col-md-12 -->


            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /.counter-section -->



    <!-- Call to action - two section -->
    <section class="cta-two-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <h3>Têm uma idéia?</h3>
                    <p>Gostaria de colocar em prática uma idéia inovadora? Vamos trabalhar juntos!</p>
                </div>
                <div class="col-sm-3">
                    <a href="http://gestorderestaurantes.com.br/#contact" onclick='descricaoContato("Idéias", 3);' class="btn btn-overcolor">Envie sua idéia</a>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>



    <!-- Contact section -->
    <section id="contact" class="contact content-section no-bottom-pad">
        <div class="container">
            <div class="row text-center">
                <script>
                    function descricaoContato(Titulo, Posicao) {
                        debugger;
                        document.getElementById("contato").innerText = "";
                        document.getElementById("mensagemConato").innerText = "";

                        if (Posicao == 0) {
                            document.getElementById("contato").innerText = Titulo;
                            document.getElementById("mensagemConato").innerText = '<%=GetGlobalResourceObject("Return","Duvidas") %>';
                        }
                        else if (Posicao == 1) {
                            document.getElementById("contato").innerText = Titulo;
                            document.getElementById("mensagemConato").innerText = '<%=GetGlobalResourceObject("Return","Sugestoes") %>';
                        }
                        else if (Posicao == 2) {
                            document.getElementById("contato").innerText = Titulo;
                            document.getElementById("mensagemConato").innerText = '<%=GetGlobalResourceObject("Return","Parceiro") %>';
                        }
                        else if (Posicao == 3) {
                            document.getElementById("contato").innerText = Titulo;
                            document.getElementById("mensagemConato").innerText = '<%=GetGlobalResourceObject("Return","Ideia") %>';
                        }
                        else {

                        }
        }
                </script>
                <div class="col-md-12">
                    <h2>
                        <label id="contato">Contato</label></h2>
                    <h3 class="caption gray">
                        <label id="mensagemConato">Envie sua mensagem, retornaremos rapidamente.</label></h3>
                </div>
                <!-- /.col-md-12 -->

            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->

        <div class="container">
            <div class="row form-container">
                <div class="col-md-7 contact-form">
                    <h3>
                        <label id="lbRetornoContato">Sua Mensagem</label></h3>
                    <div class="ajax-form" id="contactForm">
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Seu Nome." value="" required />
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" name="email" placeholder="E-Mail." value="" required />
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control" id="phone" name="phone" placeholder="Telefone. Dite somente números com DDD (11994219450)" value="" required />
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="4" id="message" name="message" placeholder="Mensagem.." required></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-default" onclick='validarContato(); return false;'><i class="fa fa-paper-plane fa-fw"></i>Enviar</button>
                        </div>
                        <script>

                            function validarContato() {
                                debugger;
                                PageMethods.enviarContato(document.getElementById("name").value, document.getElementById("email").value,
                                                           document.getElementById("phone").value, document.getElementById("message").value,
                                                           document.getElementById("contato").innerText,
                                                           OnGetMessageSuccess2, OnGetMessageFailure2);
                            }

                            function OnGetMessageSuccess2(result, userContext, methodName) {
                                debugger;
                                if (result == "name") {
                                    document.getElementById("name").style.borderColor = "red";
                                    document.getElementById("name").value = "";
                                    document.getElementById("name").placeholder = "Nome. *Campo Obrigatório. Digite seu nome completo";
                                }
                                else if (result == "email") {
                                    document.getElementById("email").style.borderColor = "green";

                                    document.getElementById("email").style.borderColor = "red";
                                    document.getElementById("email").value = "";
                                    document.getElementById("email").placeholder = "E-Mail. *Campo Obrigatório. Digite seu email sem espaços";
                                }
                                else if (result == "phone") {
                                    document.getElementById("phone").style.borderColor = "green";

                                    document.getElementById("phone").style.borderColor = "red";
                                    document.getElementById("phone").value = "";
                                    document.getElementById("phone").placeholder = "Fone. *Campo Obrigatório. Digite somente os números com ddd";
                                }
                                else if (result == "message") {
                                    document.getElementById("message").style.borderColor = "green";

                                    document.getElementById("message").style.borderColor = "red";
                                    document.getElementById("message").value = "";
                                    document.getElementById("message").placeholder = "Mensagem. *Campo Obrigatório.";
                                }
                                else {
                                    document.getElementById("name").style.borderColor = "green";
                                    document.getElementById("email").style.borderColor = "green";
                                    document.getElementById("phone").style.borderColor = "green";
                                    document.getElementById("message").style.borderColor = "green";

                                    document.getElementById("name").value = "";
                                    document.getElementById("email").value = "";
                                    document.getElementById("phone").value = "";
                                    document.getElementById("message").value = "";

                                    document.getElementById("lbRetornoContato").innerText = result;
                                    document.getElementById("lbRetornoContato").style.color = "green";
                                }

                            }
                            function OnGetMessageFailure2(error, userContext, methodName) {
                                document.getElementById("lbRetornoContato").innerText = error.get_message();
                                document.getElementById("lbRetornoContato").style.color = "red";
                            }

                        </script>
                    </div>
                </div>
                <!-- /.contact-form -->

                <div class="col-md-5 contact-address">
                    <h3>Nossos Números</h3>
                    <p>
                        Atendemos em todo o território nacional!

                        <br />
                        <br />

                        Através de nosso suporte online podemos colocar o sistema em atividade
                        em qualquer local do país. Ou se desejar temos opção de instalação presencial (consulte valores).

                        <br />
                        <br />

                        Aguardamos seu contato!
                    </p>
                    <ul>
                        <li><span style="padding: 10px">Envie uma mensagem e um de nosso consultores irá retornar rapidamente. </span></li>
                        <%--                        <li><span style="padding:10px">Cascavel - PR / Edvaldo:</span> 45 9966 7148</li>
                        <li><span style="padding:10px">Chapecó - SC / Ernani:</span> 49 9156 9956</li>
                        <li><span style="padding:10px">Maringá - PR / Lucas:</span> 44 9147 5691</li>
                        <li><span style="padding:10px">Toledo - PR / Flávio:</span> 45 9841 9870</li>
                        <li><span style="padding:10px">Toledo - PR / Angela:</span> 45 9803 3441</li>
                        <li><span style="padding:10px">Campo Grande - MS / Eliane:</span> Contate outro Analista!</li>--%>
                    </ul>
                </div>
                <!-- /.contact-address -->

            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->


    </section>
            <!--pop-up-grid-->
            <script src="http://gestorderestaurantes.com.br/c_estilo/script/jquery.magnific-popup.js" type="text/javascript"></script>

            <script>
                $(document).ready(function () {
                    $('.popup-with-zoom-anim').magnificPopup({
                        type: 'inline',
                        fixedContentPos: false,
                        fixedBgPos: true,
                        overflowY: 'auto',
                        closeBtnInside: true,
                        preloader: false,
                        midClick: true,
                        removalDelay: 300,
                        mainClass: 'my-mfp-zoom-in'
                    });
                    $('.popup-with-zoom-anim2').magnificPopup({
                        type: 'inline',
                        fixedContentPos: false,
                        fixedBgPos: true,
                        overflowY: 'auto',
                        closeBtnInside: true,
                        preloader: false,
                        midClick: true,
                        removalDelay: 300,
                        mainClass: 'my-mfp-zoom-in'
                    });
                });
            </script>
            <div id="popup">
                <div id="small-dialog" class="mfp-hide">
                    <div class="pop_up">

                        <script>
                            function validarAdquirir() {
                                debugger;
                                document.getElementById("lbRetornoAdquirir").innerText.replace(document.getElementById("lbRetornoAdquirir").innerText, "");
                                PageMethods.enviarAdquirir(document.getElementById("txbRazaoSocialadquirir").value, document.getElementById("txbCNPJadquirir").value,
                                                           document.getElementById("txbEmailadquirir").value, document.getElementById("txbFoneadquirir").value,
                                                           document.getElementById("txbSolicitanteadquirir").value, document.getElementById("txbMensagemadquirir").value, document.getElementById("lbRetornoTituloAdquirir").innerText,
                                                           OnGetMessageSuccess, OnGetMessageFailure);
                            }
                            function OnGetMessageSuccess(result, userContext, methodName) {
                                debugger;
                                if (result == "razaoSocial") {
                                    document.getElementById("txbRazaoSocialadquirir").style.borderColor = "red";
                                    document.getElementById("txbRazaoSocialadquirir").value = "";
                                    document.getElementById("txbRazaoSocialadquirir").placeholder = "Razão Social. *Campo Obrigatório";
                                }
                                else if (result == "CNPJ") {
                                    document.getElementById("txbRazaoSocialadquirir").style.borderColor = "green";

                                    document.getElementById("txbCNPJadquirir").style.borderColor = "red";
                                    document.getElementById("txbCNPJadquirir").value = "";
                                    document.getElementById("txbCNPJadquirir").placeholder = "CNPJ. *Campo Obrigatório. Digite somente os números";
                                }
                                else if (result == "email") {
                                    document.getElementById("txbCNPJadquirir").style.borderColor = "green";

                                    document.getElementById("txbEmailadquirir").style.borderColor = "red";
                                    document.getElementById("txbEmailadquirir").value = "";
                                    document.getElementById("txbEmailadquirir").placeholder = "Email. *Campo Obrigatório. contato@email.com.br";
                                }
                                else if (result == "telefone") {
                                    document.getElementById("txbEmailadquirir").style.borderColor = "green";

                                    document.getElementById("txbFoneadquirir").style.borderColor = "red";
                                    document.getElementById("txbFoneadquirir").value = "";
                                    document.getElementById("txbFoneadquirir").placeholder = "Telefone. *Campo Obrigatório. Digite somente os números com ddd";
                                }
                                else if (result == "solicitante") {
                                    document.getElementById("txbFoneadquirir").style.borderColor = "green";

                                    document.getElementById("txbSolicitanteadquirir").style.borderColor = "red";
                                    document.getElementById("txbSolicitanteadquirir").value = "";
                                    document.getElementById("txbSolicitanteadquirir").placeholder = "Solicitante. *Campo Obrigatório.";
                                }
                                else if (result == "mensagem") {
                                    document.getElementById("txbSolicitanteadquirir").style.borderColor = "green";

                                    document.getElementById("txbMensagemadquirir").style.borderColor = "red";
                                    document.getElementById("txbMensagemadquirir").value = "";
                                    document.getElementById("txbMensagemadquirir").placeholder = "Mensagem. *Campo Obrigatório.";
                                }
                                else {
                                    document.getElementById("txbRazaoSocialadquirir").style.borderColor = "green";
                                    document.getElementById("txbCNPJadquirir").style.borderColor = "green";
                                    document.getElementById("txbEmailadquirir").style.borderColor = "green";
                                    document.getElementById("txbFoneadquirir").style.borderColor = "green";
                                    document.getElementById("txbSolicitanteadquirir").style.borderColor = "green";
                                    document.getElementById("txbMensagemadquirir").style.borderColor = "green";

                                    document.getElementById("txbRazaoSocialadquirir").value = "";
                                    document.getElementById("txbCNPJadquirir").value = "";
                                    document.getElementById("txbEmailadquirir").value = "";
                                    document.getElementById("txbFoneadquirir").value = "";
                                    document.getElementById("txbSolicitanteadquirir").value = "";
                                    document.getElementById("txbMensagemadquirir").value = "";

                                    document.getElementById("lbRetornoAdquirir").innerText = result;
                                    document.getElementById("lbRetornoAdquirir").style.color = "green";
                                }

                            }
                            function OnGetMessageFailure(error, userContext, methodName) {
                                document.getElementById("lbRetornoAdquirir").innerText = error.get_message();
                                document.getElementById("txbRazaoSocialadquirir").value = document.getElementById("txbCNPJadquirir").value,
                                                           document.getElementById("txbEmailadquirir").value = document.getElementById("txbFoneadquirir").value,
                                                           document.getElementById("txbSolicitanteadquirir").value = document.getElementById("txbMensagemadquirir").value = "";
                            }
                        </script>


                        <div class="col-md-12">
                            <h3><label id="lbRetornoTituloAdquirir">Adquirir - Gestor de Restaurantes</label></h3>
                            <h4>
                                <label id="lbRetornoAdquirir"></label>
                            </h4>
                            <div class="ajax-form" id="contactForm2">
                                <div class="form-group">
                                    <input type="text" name="razaoSocial" class="form-control" id="txbRazaoSocialadquirir" placeholder="Razão Social..." />
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" id="txbCNPJadquirir" placeholder="CNPJ... Digita Apenas os Números" />
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="txbEmailadquirir" placeholder="E-Mail..." />
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" id="txbFoneadquirir" placeholder="Telefone..." />
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="txbSolicitanteadquirir" placeholder="Seu Nome..." />
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="4" id="txbMensagemadquirir" placeholder="Sua Mensagem..."></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" id="btnEnviaradquirir" name="submit" class="btn btn-default" onclick='validarAdquirir(); return false;'><i class="fa fa-paper-plane fa-fw"></i>Enviar </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <script>
        $(document).ready(function () {
            $('.popup-with-zoom-anim').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in'
            });
            $('.popup-with-zoom-anim2').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in'
            });
        });
    </script>

    <script>
        function validarAdquirir() {
            debugger;
            document.getElementById("lbRetornoAdquirir").innerText.replace(document.getElementById("lbRetornoAdquirir").innerText, "");
            PageMethods.enviarAdquirir(document.getElementById("txbRazaoSocialadquirir").value, document.getElementById("txbCNPJadquirir").value,
                                       document.getElementById("txbEmailadquirir").value, document.getElementById("txbFoneadquirir").value,
                                       document.getElementById("txbSolicitanteadquirir").value, document.getElementById("txbMensagemadquirir").value, document.getElementById("lbRetornoTituloAdquirir").innerText,
                                       OnGetMessageSuccess, OnGetMessageFailure);
        }
        function OnGetMessageSuccess(result, userContext, methodName) {
            debugger;
            if (result == "razaoSocial") {
                document.getElementById("txbRazaoSocialadquirir").style.borderColor = "red";
                document.getElementById("txbRazaoSocialadquirir").value = "";
                document.getElementById("txbRazaoSocialadquirir").placeholder = "Razão Social. *Campo Obrigatório";
            }
            else if (result == "CNPJ") {
                document.getElementById("txbRazaoSocialadquirir").style.borderColor = "green";

                document.getElementById("txbCNPJadquirir").style.borderColor = "red";
                document.getElementById("txbCNPJadquirir").value = "";
                document.getElementById("txbCNPJadquirir").placeholder = "CNPJ. *Campo Obrigatório. Digite somente os números";
            }
            else if (result == "email") {
                document.getElementById("txbCNPJadquirir").style.borderColor = "green";

                document.getElementById("txbEmailadquirir").style.borderColor = "red";
                document.getElementById("txbEmailadquirir").value = "";
                document.getElementById("txbEmailadquirir").placeholder = "Email. *Campo Obrigatório. contato@email.com.br";
            }
            else if (result == "telefone") {
                document.getElementById("txbEmailadquirir").style.borderColor = "green";

                document.getElementById("txbFoneadquirir").style.borderColor = "red";
                document.getElementById("txbFoneadquirir").value = "";
                document.getElementById("txbFoneadquirir").placeholder = "Telefone. *Campo Obrigatório. Digite somente os números com ddd";
            }
            else if (result == "solicitante") {
                document.getElementById("txbFoneadquirir").style.borderColor = "green";

                document.getElementById("txbSolicitanteadquirir").style.borderColor = "red";
                document.getElementById("txbSolicitanteadquirir").value = "";
                document.getElementById("txbSolicitanteadquirir").placeholder = "Solicitante. *Campo Obrigatório.";
            }
            else if (result == "mensagem") {
                document.getElementById("txbSolicitanteadquirir").style.borderColor = "green";

                document.getElementById("txbMensagemadquirir").style.borderColor = "red";
                document.getElementById("txbMensagemadquirir").value = "";
                document.getElementById("txbMensagemadquirir").placeholder = "Mensagem. *Campo Obrigatório.";
            }
            else {
                document.getElementById("txbRazaoSocialadquirir").style.borderColor = "green";
                document.getElementById("txbCNPJadquirir").style.borderColor = "green";
                document.getElementById("txbEmailadquirir").style.borderColor = "green";
                document.getElementById("txbFoneadquirir").style.borderColor = "green";
                document.getElementById("txbSolicitanteadquirir").style.borderColor = "green";
                document.getElementById("txbMensagemadquirir").style.borderColor = "green";

                document.getElementById("txbRazaoSocialadquirir").value = "";
                document.getElementById("txbCNPJadquirir").value = "";
                document.getElementById("txbEmailadquirir").value = "";
                document.getElementById("txbFoneadquirir").value = "";
                document.getElementById("txbSolicitanteadquirir").value = "";
                document.getElementById("txbMensagemadquirir").value = "";

                document.getElementById("lbRetornoAdquirir").innerText = result;
                document.getElementById("lbRetornoAdquirir").style.color = "green";
            }

        }
        function OnGetMessageFailure(error, userContext, methodName) {
            document.getElementById("lbRetornoAdquirir").innerText = error.get_message();
            document.getElementById("txbRazaoSocialadquirir").value = document.getElementById("txbCNPJadquirir").value,
                                       document.getElementById("txbEmailadquirir").value = document.getElementById("txbFoneadquirir").value,
                                       document.getElementById("txbSolicitanteadquirir").value = document.getElementById("txbMensagemadquirir").value = "";
        }
    </script>

    <script>
        function descricaoTestePoupUp(Titulo) {
            debugger;
            document.getElementById("lbRetornoTituloAdquirir").innerText = Titulo;
            document.getElementById("lbRetornoAdquirir").innerText = "";

            document.getElementById("txbRazaoSocialadquirir").style.borderColor = "gray";
            document.getElementById("txbCNPJadquirir").style.borderColor = "gray";
            document.getElementById("txbEmailadquirir").style.borderColor = "gray";
            document.getElementById("txbFoneadquirir").style.borderColor = "gray";
            document.getElementById("txbSolicitanteadquirir").style.borderColor = "gray";
            document.getElementById("txbMensagemadquirir").style.borderColor = "gray";

            document.getElementById("txbRazaoSocialadquirir").value = "";
            document.getElementById("txbCNPJadquirir").value = "";
            document.getElementById("txbEmailadquirir").value = "";
            document.getElementById("txbFoneadquirir").value = "";
            document.getElementById("txbSolicitanteadquirir").value = "";
            document.getElementById("txbMensagemadquirir").value = "";
        }

        function descricaoPoupUp(Condicao, Posicao) {
            debugger;
            document.getElementById("lbTitulaPoupUp").innerText = "";
            document.getElementById("lbDescricaoPoupUp").innerText = "";

            if (Posicao == 1) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","Instalação") %>';
            }
            else if (Posicao == 2) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","Site") %>';
            }
            else if (Posicao == 3) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","MultiLojas") %>';
            }
            else if (Posicao == 4) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","NFe") %>';
            }
            else if (Posicao == 5) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","UsuariosClientes") %>';
            }
            else if (Posicao == 6) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","Vendas") %>';
            }
            else if (Posicao == 7) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","PedidoEletronico") %>';
            }
            else if (Posicao == 8) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","ControleCaixa") %>';
            }
            else if (Posicao == 9) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","ImpressoraTermica") %>';
            }
            else if (Posicao == 10) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","EntradaEstoque") %>';
            }
            else if (Posicao == 11) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","SuporteIlimitado") %>';
            }
            else if (Posicao == 12) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","Personalizacao") %>';
            }
            else if (Posicao == 13) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","Personalizacao") %>';
            }
            else if (Posicao == 14) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","PlanoProfissionalPersonalizacao") %>';
            }
            else if (Posicao == 15) {
                document.getElementById("lbTitulaPoupUp").innerText = Condicao;
                document.getElementById("lbDescricaoPoupUp").innerText = '<%=GetGlobalResourceObject("Return","Atencao") %>';
            }
        }
    </script>

</asp:Content>
